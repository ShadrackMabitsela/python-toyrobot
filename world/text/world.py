from typing import *
import world.obstacles as obstacles

# variables tracking position and direction
position_x = 0
position_y = 0
directions = ['forward', 'right', 'back', 'left']
current_direction_index = 0

# area limit variables
min_y, max_y = -200, 200
min_x, max_x = -100, 100

# To check if the path is blocked.
path_is_blocked = False


def show_position(robot_name: str) -> None:
    """
    The show_position() function prints out the robots name
    and its current cartesian position.\n
    Parameters: 1\t
    parameter1: robot_name: str.\n
    Prints Out:\t\t
    str: The robots name and its current cartesian position (x, y).\n
    Returns: \t
    None: Returns the Standard None.\t
    """
    print(' > '+robot_name+' now at position (' +
          str(position_x)+','+str(position_y)+').')
    return None


def is_position_allowed(new_x: int, new_y: int) -> bool:
    """
    The is_position_allowed() function checks if the new position
    will still fall within the max area limit.\n
    Parameters: 2\t
    parameter1: new_x: int, the new/proposed x position.\t
    parameter2: new_y: int, the new/proposed y position.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    bool: True if allowed, i.e. it falls in the allowed area, else False.
    """
    return min_x <= new_x <= max_x and min_y <= new_y <= max_y


def update_position(steps: int) -> bool:
    """
    The update_position() function updates the current x and y positions
    given the current direction, and specific nr of steps.\n
    Parameters: 1\t
    parameter1: steps: int.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    bool: True if the position was updated, else False.
    """
    global position_x, position_y, directions, current_direction_index, path_is_blocked
    new_x = position_x
    new_y = position_y

    if directions[current_direction_index] == 'forward':
        new_y = new_y + steps
    elif directions[current_direction_index] == 'right':
        new_x = new_x + steps
    elif directions[current_direction_index] == 'back':
        new_y = new_y - steps
    elif directions[current_direction_index] == 'left':
        new_x = new_x - steps

    if is_position_allowed(new_x, new_y):
        if obstacles.is_path_blocked(position_x, position_y, new_x, new_y):
            path_is_blocked = True
        else:
            position_y = new_y
            position_x = new_x
            return True
    return False


def do_forward(robot_name: str, steps: int) -> Tuple[bool, str]:
    """
    The do_forward() function moves the robot forward the number of steps given.\n
    Parameters: 2\t
    parameter1: robot_name: str.\t
    parameter2: steps: int.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: (True, 'forward output text')
    """
    global path_is_blocked
    if update_position(steps) and (path_is_blocked == False):
        return True, ' > '+robot_name+' moved forward by '+str(steps)+' steps.'
    elif (update_position(steps) == False) and (path_is_blocked == True):
        path_is_blocked = False
        return True, ''+robot_name+': Sorry, there is an obstacle in the way.'
    else:
        return True, ''+robot_name+': Sorry, I cannot go outside my safe zone.'


def do_back(robot_name: str, steps: int) -> Tuple[bool, str]:
    """
    The do_back() function moves the robot back the number of steps given.\n
    Parameters: 2\t
    parameter1: robot_name: str.\t
    parameter2: steps: int.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: (True, 'backward output text')
    """
    global path_is_blocked
    if update_position(-steps) and (path_is_blocked == False):
        return True, ' > '+robot_name+' moved back by '+str(steps)+' steps.'
    elif (update_position(-steps) == False) and (path_is_blocked == True):
        path_is_blocked = False
        return True, ''+robot_name+': Sorry, there is an obstacle in the way.'
    else:
        return True, ''+robot_name+': Sorry, I cannot go outside my safe zone.'


def do_right_turn(robot_name: str) -> Tuple[bool, str]:
    """
    The do_right_turn() function turns the robot 90 degrees to the right.\n
    Parameters: 1\t
    parameter1: robot_name: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: (True, 'right turn output text')
    """
    global current_direction_index
    current_direction_index += 1
    if current_direction_index > 3:
        current_direction_index = 0
    return True, ' > '+robot_name+' turned right.'


def do_left_turn(robot_name: str) -> Tuple[bool, str]:
    """
    The do_left_turn() function turns the robot 90 degrees to the left.\n
    Parameters: 1\t
    parameter1: robot_name: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: (True, 'left turn output text')
    """
    global current_direction_index
    current_direction_index -= 1
    if current_direction_index < 0:
        current_direction_index = 3
    return True, ' > '+robot_name+' turned left.'


def do_sprint(robot_name: str, steps: int) -> Tuple[bool, str]:
    """
    The do_sprint() function sprints the robot, i.e. let it go forward steps +
    (steps-1) + (steps-2) + .. + 1 number of steps, in iterations.\n
    Parameters: 2\t
    parameter1: robot_name: str.\t
    parameter2: steps: int.\n
    Prints Out:\t\t
    str: The number of steps forward the robot took.\n
    Returns:\t
    tuple: (True, forward output)
    """
    if steps == 1:
        return do_forward(robot_name, 1)
    else:
        (do_next, command_output) = do_forward(robot_name, steps)
        print(command_output)
        return do_sprint(robot_name, steps - 1)
