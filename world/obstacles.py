import random as random
from typing import List, Tuple
 

list_of_obstacles = []


#  Step 4
def get_obstacles() -> List[Tuple[int, int]]:
    """
    The get_obstacles() function generates a list of random length,
    containing (random(x), random(y)) coordinates in tuple form.\n
    Parameters:\t\t
    None: No parameters.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns: \t
    List: Returns a list containing (x, y) coordinate tuples.\t
    """
    number_of_obstacles = random.randint(0, 10)
    for _ in range(number_of_obstacles):
        list_of_obstacles.append(
            (random.randint(-110, 107), random.randint(-211, 206)))
    return list_of_obstacles


def is_position_blocked(x: int, y: int) -> bool:
    """
    The is_position_blocked() function checks if the (x, y)
    position falls inside an obstacle.\n
    Parameters: 2\t\t
    parameter1: x: int.\t\t
    parameter2: y: int.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns: \t
    bool: Returns a boolean.\t
    """
    if len(list_of_obstacles) == 0:
        return False
    for o_x1, o_y1 in list_of_obstacles:
        if (x >= o_x1 and x <= (o_x1 + 4)) and (y >= o_y1 and y <= (o_y1 + 4)):
            return True
    return False


def is_path_blocked(x1: int, y1: int, x2: int, y2: int) -> bool:
    """
    The is_path_blocked() function checks if there is an obstacle
    in the line between the coordinates (x1, y1) and (x2, y2).\n
    Parameters: 4\t\t
    parameter1: x1: int.\t\t
    parameter2: y1: int.\t\t
    parameter3: x2: int.\t\t
    parameter4: y2: int.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns: \t
    bool: Returns a boolean.\t
    """
    if len(list_of_obstacles) == 0:
        return False
    if x1 == x2:
        if y1 >= y2:
            while y1 >= y2:
                if is_position_blocked(x1, y1):
                    return True
                y1 -= 1
        elif y1 <= y2:
            while y1 <= y2:
                if is_position_blocked(x1, y1):
                    return True
                y1 += 1
    elif y1 == y2:
        if x1 >= x2:
            while x1 >= x2:
                if is_position_blocked(x1, y1):
                    return True
                x1 -= 1
        elif x1 <= x2:
            while x1 <= x2:
                if is_position_blocked(x1, y1):
                    return True
                x1 += 1
    return False


# print(str(get_obstacles()))
# print(str(is_position_blocked(-32, -155)))
# print(str(is_path_blocked(40, 20, 40, 180)))
