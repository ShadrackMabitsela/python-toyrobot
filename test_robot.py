import unittest
from unittest.mock import patch
from io import StringIO
from test_base import captured_io
import world.obstacles as obstacles
import world.text.world as world
import robot


class TestFunctions(unittest.TestCase):

    # Step 1 Test No Arguments.
        # TDD RED - Make it FAIL.
    def test_no_args_fail(self):
        obstacles.list_of_obstacles = []
        with captured_io(StringIO('BOT\nFORWARD\nBacK\nsprint\noFf\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next? BOT: Sorry, I did not understand 'FORWARD'.
BOT: What must I do next? BOT: Sorry, I did not understand 'BacK'.
BOT: What must I do next? BOT: Sorry, I did not understand 'sprint'.
BOT: What must I do next? BOT: Shutting down..""", output)


    # Step 1 Test Replay Silent Command.
        # TDD RED - Make it FAIL.
    def test_replay_silent_fail(self):
        with captured_io(StringIO('BOT\nright\nback 20\nright\nright\nREPLAY silnt\noff\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (0,0).
BOT: What must I do next?  > BOT moved back by 20 steps.
 > BOT now at position (-20,0).
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (-20,0).
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (-20,0).
BOT: What must I do next? BOT: Sorry, I did not understand 'REPLAY silnt'.
BOT: What must I do next? BOT: Shutting down..""", output)


        # TDD GREEN - Make it PASS.
    def test_replay_silent_pass(self):
        with captured_io(StringIO('BOT\nleft\nforward 50\nleft\nback 50\nreplay SiLenT\nOfF')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,0).
BOT: What must I do next?  > BOT moved forward by 50 steps.
 > BOT now at position (-50,0).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (-50,0).
BOT: What must I do next?  > BOT moved back by 50 steps.
 > BOT now at position (-50,50).
BOT: What must I do next?  > BOT replayed 4 commands silently.
 > BOT now at position (0,0).
BOT: What must I do next? BOT: Shutting down..""", output)


    # Step 1 Test Replay Reversed Command.
        # TDD RED - Make it FAIL.
    def test_replay_reversed_fail(self):
        with captured_io(StringIO('BOT\nright\nback 80\nforward 10\nsprint 5\nreplay revercd\nOFF\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (0,0).
BOT: What must I do next?  > BOT moved back by 80 steps.
 > BOT now at position (-80,0).
BOT: What must I do next?  > BOT moved forward by 10 steps.
 > BOT now at position (-70,0).
BOT: What must I do next?  > BOT moved forward by 5 steps.
 > BOT moved forward by 4 steps.
 > BOT moved forward by 3 steps.
 > BOT moved forward by 2 steps.
 > BOT moved forward by 1 steps.
 > BOT now at position (-55,0).
BOT: What must I do next? BOT: Sorry, I did not understand 'replay revercd'.
BOT: What must I do next? BOT: Shutting down..""", output)


        # TDD GREEN - Make it PASS.
    def test_replay_reversed_pass(self):
        with captured_io(StringIO('BOT\nright\nback 20\nright\nsprint 5\nreplay reversed\noff\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (0,0).
BOT: What must I do next?  > BOT moved back by 20 steps.
 > BOT now at position (-20,0).
BOT: What must I do next?  > BOT turned right.
 > BOT now at position (-20,0).
BOT: What must I do next?  > BOT moved forward by 5 steps.
 > BOT moved forward by 4 steps.
 > BOT moved forward by 3 steps.
 > BOT moved forward by 2 steps.
 > BOT moved forward by 1 steps.
 > BOT now at position (-20,-15).
BOT: What must I do next?  > BOT moved forward by 5 steps.
 > BOT moved forward by 4 steps.
 > BOT moved forward by 3 steps.
 > BOT moved forward by 2 steps.
 > BOT moved forward by 1 steps.
 > BOT now at position (-20,-30).
 > BOT turned right.
 > BOT now at position (-20,-30).
 > BOT moved back by 20 steps.
 > BOT now at position (0,-30).
 > BOT turned right.
 > BOT now at position (0,-30).
 > BOT replayed 4 commands in reverse.
 > BOT now at position (0,-30).
BOT: What must I do next? BOT: Shutting down..""", output)



    # Step 1 Test Replay Reversed Command Silent.
            # TDD RED - Make it FAIL.
    def test_replay_reversed_silent_fail(self):
        with captured_io(StringIO('BOT\nSPRINT 8\nLEFT\nleFt\nreplay revercd SILENT\noFf\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT moved forward by 8 steps.
 > BOT moved forward by 7 steps.
 > BOT moved forward by 6 steps.
 > BOT moved forward by 5 steps.
 > BOT moved forward by 4 steps.
 > BOT moved forward by 3 steps.
 > BOT moved forward by 2 steps.
 > BOT moved forward by 1 steps.
 > BOT now at position (0,36).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,36).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,36).
BOT: What must I do next? BOT: Sorry, I did not understand 'replay revercd SILENT'.
BOT: What must I do next? BOT: Shutting down..""", output)

            # TDD GREEN - Make it PASS.
    def test_replay_reversed_silent_pass(self):
        with captured_io(StringIO('BOT\nforward 8\nLEFT\nleFt\nreplay ReverseD SILENT\noFf\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT moved forward by 8 steps.
 > BOT now at position (0,8).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,8).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,8).
BOT: What must I do next?  > BOT replayed 3 commands in reverse silently.
 > BOT now at position (0,16).
BOT: What must I do next? BOT: Shutting down..""", output)


# Step 1 Test Replay Reversed Command Silent.
            # TDD RED - Make it FAIL.
    def test_replay_limit_range_of_commands_fail(self):
        with captured_io(StringIO('BOT\nback 67\nLEFT\nforward 84\nreplay 3+2\noFf\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT moved back by 67 steps.
 > BOT now at position (0,-67).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,-67).
BOT: What must I do next?  > BOT moved forward by 84 steps.
 > BOT now at position (-84,-67).
BOT: What must I do next? BOT: Sorry, I did not understand 'replay 3+2'.
BOT: What must I do next? BOT: Shutting down..""", output)


            # TDD GREEN - Make it PASS.
    def test_replay_limit_range_of_commands_pass(self):
        with captured_io(StringIO('BOT\nback 67\nLEFT\nforward 84\nreplay 3-2\noFf\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? BOT: Hello kiddo!
BOT: What must I do next?  > BOT moved back by 67 steps.
 > BOT now at position (0,-67).
BOT: What must I do next?  > BOT turned left.
 > BOT now at position (0,-67).
BOT: What must I do next?  > BOT moved forward by 84 steps.
 > BOT now at position (-84,-67).
BOT: What must I do next?  > BOT moved back by 67 steps.
 > BOT now at position (-17,-67).
 > BOT replayed 1 commands.
 > BOT now at position (-17,-67).
BOT: What must I do next? BOT: Shutting down..""", output)