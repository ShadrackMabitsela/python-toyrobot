import unittest
from unittest.mock import patch
from io import StringIO
from test_base import captured_io
from world import obstacles

class TestFunctions(unittest.TestCase):
    # Step 4 Test obstacles functions.
        # TDD RED - Make it FAIL.
    def test_get_obstacles_fail1(self):
        self.assertFalse(type(obstacles.get_obstacles()) == dict)


        # TDD GREEN - Make it PASS.
    def test_get_obstacles_pass1(self):
        self.assertTrue(type(obstacles.get_obstacles()) == list)
    
    def test_get_obstacles_pass2(self):
        self.assertTrue(len(obstacles.get_obstacles()) >= 0)


        # TDD RED - Make it FAIL.
    def test_is_position_blocked_fail1(self):
        obstacles.list_of_obstacles = []
        self.assertFalse(obstacles.is_position_blocked(10, 55) == True)

    def test_is_position_blocked_fail2(self):
        obstacles.list_of_obstacles = [(-34, -157)]
        self.assertFalse(obstacles.is_position_blocked(-36, -155) == True)

    
        # TDD RED - Make it PASS.
    def test_is_position_blocked_pass1(self):
        obstacles.list_of_obstacles = [(85, 190)]
        self.assertTrue(obstacles.is_position_blocked(87, 192) == True)


    def test_is_position_blocked_pass2(self):
        obstacles.list_of_obstacles = [(-99, -190)]
        self.assertTrue(obstacles.is_position_blocked(-97, -188) == True)


    def test_is_position_blocked_pass3(self):
        obstacles.list_of_obstacles = [(77, -112)]
        self.assertTrue(obstacles.is_position_blocked(79, -110) == True)


        # TDD RED - Make it FAIL.
    def test_is_path_blocked_fail1(self):
        obstacles.list_of_obstacles = [(-76, 32), (4, -121), (45, -40)]
        self.assertFalse(obstacles.is_path_blocked(79, -110, 79, 17) == True)


    def test_is_path_blocked_fail2(self):
        obstacles.list_of_obstacles = [(14, -159), (-55, -198), (63, 114)]
        self.assertFalse(obstacles.is_path_blocked(-80, -180, 80, -180) == True)


    def test_is_path_blocked_fail3(self):
        obstacles.list_of_obstacles = [(53, 21), (-92, 144), (26, 9), (27, -10)]
        self.assertFalse(obstacles.is_path_blocked(-40, -101, -40, 160) == True)


        # TDD RED - Make it PASS.
    def test_is_path_blocked_pass1(self):
        obstacles.list_of_obstacles = [(42, -149), (23, 79), (55, 159)]
        self.assertTrue(obstacles.is_path_blocked(25, -110, 25, 93) == True)


    def test_is_path_blocked_pass2(self):
        obstacles.list_of_obstacles = [(-53, -185), (83, 7), (-66, -175), (-15, -91)]
        self.assertTrue(obstacles.is_path_blocked(-13, -89, 80, -89) == True)


    def test_is_path_blocked_pass3(self):
        obstacles.list_of_obstacles = [(81, -103), (-11, 39), (71, 142), (-24, 90)]
        self.assertTrue(obstacles.is_path_blocked(-68, 144, 84, 144) == True)
