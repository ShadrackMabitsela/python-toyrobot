from typing import Tuple, List
import sys
import world.obstacles as obstacles


#  Step 3
if len(sys.argv) > 1 and sys.argv[1].lower() == 'text':
    obstacles.get_obstacles()
    from world.text import world as world
elif len(sys.argv) > 1 and sys.argv[1].lower() == 'turtle':
    obstacles.get_obstacles()
    from world.turtle import world as world
else:
    obstacles.get_obstacles()
    from world.text import world as world


# list of valid command names
valid_commands = ['off', 'help', 'replay',
                  'forward', 'back', 'right', 'left', 'sprint']
move_commands = valid_commands[3:]

# commands history
history = []

# chosen_module
chosen_module = ''


def get_robot_name() -> str:
    """
    The get_robot_name() function asks the user to give the robot a name.\n
    Parameters:\t\t
    None: No parameters.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    str: Returns the str input name given to the robot.
    """
    name = input("What do you want to name your robot? ")
    while len(name) == 0:
        name = input("What do you want to name your robot? ")
    return name


def get_command(robot_name: str) -> str:
    """
    The get_command() function asks the user for a command, and validates it as well.\n
    Parameters: 1\t
    parameter1: robot_name: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    str: Returns a valid command.
    """
    prompt = ''+robot_name+': What must I do next? '
    command = input(prompt)
    while len(command) == 0 or not valid_command(command):
        output(robot_name, "Sorry, I did not understand '"+command+"'.")
        command = input(prompt)
    return command.lower()


def split_command_input(command: str) -> Tuple[str, str]:
    """
    The split_command_input() function splits the string at the first space
    character, to get the actual command, as well as the argument(s) for the command.\n
    Parameters: 1\t
    parameter1: command: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: (command, argument1).
    """
    args = command.split(' ', 1)
    if len(args) > 1:
        return args[0], args[1]
    return args[0], ''


def is_int(value: str) -> bool:
    """
    The is_int() function tests if the string value is an int or not.\n
    Parameters: 1\t
    parameter1: value: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t\t
    bool: True if value is an int, else it returns False.
    """
    try:
        int(value)
        return True
    except ValueError:
        return False


def valid_command(command: str) -> bool:
    """
    The valid_command() function tests if the input is a valid command.\n
    It also checks if there is an argument to the command, and if it is a valid int.\n
    Parameters: 1\t
    parameter1: command: str.\n
    Prints Out:\t
    None: No print outs.\n
    Returns:\t
    bool: True if value is a valid command, else it returns false.
    """
    (command_name, arg1) = split_command_input(command)

    if command_name.lower() == 'replay':
        if len(arg1.strip()) == 0:
            return True
        elif (arg1.lower().find('silent') > -1 or arg1.lower().find('reversed') > -1) and len(arg1.lower().replace('silent', '').replace('reversed', '').strip()) == 0:
            return True
        else:
            range_args = arg1.replace('silent', '').replace('reversed', '')
            if is_int(range_args):
                return True
            else:
                range_args = range_args.split('-')
                return is_int(range_args[0]) and is_int(range_args[1]) and len(range_args) == 2
    elif command_name.lower() in ['forward', 'back', 'sprint'] and arg1 == '':
        return False
    else:
        return command_name.lower() in valid_commands and (len(arg1) == 0 or is_int(arg1))


def output(name: str, message: str) -> None:
    """
    The output() function prints out the robots name and the input message.\n
    Parameters: 2\t
    parameter1: name: str.\t
    parameter2: message: str.\n
    Prints Out:\t\t
    str: Prints out the robots name and the input message.\n
    Returns:\t
    None: Returns the Standard None.
    """
    print(''+name+": "+message)
    return None


def do_help() -> Tuple[bool, str]:
    """
    The do_help() function provides help information to the user.\n
    Parameters:\t
    None: name: No parameters.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    tuple: Returns (True, 'help text').\t\t
    True to continue execution and 'help text' to inform the user.
    """
    return True, """I can understand these commands:
OFF  - Shut down robot
HELP - provide information about commands
FORWARD - move forward by specified number of steps, e.g. 'FORWARD 10'
BACK - move backward by specified number of steps, e.g. 'BACK 10'
RIGHT - turn right by 90 degrees
LEFT - turn left by 90 degrees
SPRINT - sprint forward according to a formula
REPLAY - replays all movement commands from history [FORWARD, BACK, RIGHT, LEFT, SPRINT]
"""


def get_commands_history(reverse: bool, relativeStart: None, relativeEnd: None) -> List[str]:
    """
    The get_commands_history() retrieves the commands from history list,
    already breaking them up into (command_name, arguments) tuples.\n
    Parameters: 3\t
    parameter1: reverse: bool. If True, then reverse the list.\t
    parameter2: relativeStart: NoneType. The start index relative to the end position of command, e.g. -5 means from index len(commands)-5; None means from beginning.\t
    parameter3: relativeEnd: NoneType. The start index relative to the end position of command, e.g. -1 means from index len(commands)-1; None means to the end.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    list: (command_name, arguments) tuples.
    """
    commands_to_replay = [(name, args) for (name, args) in list(map(
        lambda command: split_command_input(command), history)) if name in move_commands]
    if reverse:
        commands_to_replay.reverse()

    range_start = len(commands_to_replay) + relativeStart if (
        relativeStart is not None and (len(commands_to_replay) + relativeStart) >= 0) else 0
    range_end = len(commands_to_replay) + relativeEnd if (relativeEnd is not None and (len(
        commands_to_replay) + relativeEnd) >= 0 and relativeEnd > relativeStart) else len(commands_to_replay)
    return commands_to_replay[range_start:range_end]


def do_replay(robot_name: str, arguments: str) -> Tuple[bool, str]:
    """
    The do_replay() function replays historic commands.\n
    Parameters: 2\t
    parameter1: robot_name: str.\t
    parameter2: arguments: str. A string containing arguments for the replay command.\n
    Prints Out:\t\t
    str: If not Silent, prints out the command output and the robots position.\n
    Returns:\t
    tuple: (True, 'output string')
    """
    silent = arguments.lower().find('silent') > -1
    reverse = arguments.lower().find('reversed') > -1
    range_args = arguments.lower().replace('silent', '').replace('reversed', '')

    range_start = None
    range_end = None

    if len(range_args.strip()) > 0:
        if is_int(range_args):
            range_start = -int(range_args)
        else:
            range_args = range_args.split('-')
            range_start = -int(range_args[0])
            range_end = -int(range_args[1])

    commands_to_replay = get_commands_history(reverse, range_start, range_end)

    for (command_name, command_arg) in commands_to_replay:
        (do_next, command_output) = call_command(
            command_name, command_arg, robot_name)
        if not silent:
            print(command_output)
            world.show_position(robot_name)

    return True, ' > '+robot_name+' replayed ' + str(len(commands_to_replay)) + ' commands' + (' in reverse' if reverse else '') + (' silently.' if silent else '.')


def call_command(command_name: str, command_arg: str, robot_name: str) -> Tuple[bool, str]:
    """
    The call_command() function executes the commands with the given arguments.\n
    Parameters: 3\t
    parameter1: command_name: str.\t
    parameter2: command_arg: str.\t
    parameter3: robot_name: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns: Two Types\t
    tuple: (True, 'output string') If command_name has a match.\t\t
    tuple: (False, NoneType). If command_name has no match.
    """
    if command_name == 'help':
        return do_help()
    elif command_name == 'forward':
        return world.do_forward(robot_name, int(command_arg))
    elif command_name == 'back':
        return world.do_back(robot_name, int(command_arg))
    elif command_name == 'right':
        return world.do_right_turn(robot_name)
    elif command_name == 'left':
        return world.do_left_turn(robot_name)
    elif command_name == 'sprint':
        return world.do_sprint(robot_name, int(command_arg))
    elif command_name == 'replay':
        return do_replay(robot_name, command_arg)
    return False, None


def handle_command(robot_name: str, command: str) -> bool:
    """
    The handle_command() function handles a command by asking different functions to handle each command.\n
    Parameters: 2\t
    parameter1: robot_name: str.\t
    parameter2: command_arg: str.\n
    Prints Out:\t\t
    str: Prints the command output.\n
    Returns:\t
    bool: do_next.\t\t
    """
    (command_name, arg) = split_command_input(command)

    if command_name == 'off':
        return False
    else:
        (do_next, command_output) = call_command(command_name, arg, robot_name)

    print(command_output)
    world.show_position(robot_name)
    add_to_history(command)

    return do_next


def add_to_history(command: str) -> None:
    """
    The add_to_history() appends the command to the history container.\n
    Parameters: 1\t
    parameter1: command: str.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    None: Returns the Standard None.
    """
    history.append(command)
    return None


def robot_start() -> None:
    """
    The robot_start() function
    is the entry point for starting the robot.\n
    Parameters:\t
    None: No parameters.\n
    Prints Out:\t\t
    None: No print outs.\n
    Returns:\t
    None: Returns the Standard None.
    """
    if len(sys.argv) == 1 or sys.argv[1].lower() == 'text':
        chosen_module = 'text'
    elif sys.argv[1].lower() == 'turtle':
        chosen_module = 'turtle'
    else:
        chosen_module = 'text'

    global history


    robot_name = get_robot_name()
    output(robot_name, "Hello kiddo!")


    if chosen_module == 'text' and len(obstacles.list_of_obstacles) != 0:
        print('There are some obstacles:')
        for obst in obstacles.list_of_obstacles:
            print(f"- At position {obst[0]},{obst[1]} (to {obst[0] + 4},{obst[1] + 4})")

    command = get_command(robot_name)
    while handle_command(robot_name, command):
        command = get_command(robot_name)

    world.position_x = 0
    world.position_y = 0
    world.current_direction_index = 0
    history = []
    obstacles.list_of_obstacles.clear()

    world.path_is_blocked = False
    if chosen_module == 'turtle':
        world.turtle_inst.home()
        world.turtle_inst.left(90)

    output(robot_name, "Shutting down..")
    return None


if __name__ == "__main__":
    robot_start()
