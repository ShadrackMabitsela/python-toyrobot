import unittest
from unittest.mock import patch
from io import StringIO
from test_base import captured_io
import world.obstacles as obstacles
import world.text.world as world
import robot


class TestFunctions(unittest.TestCase):

    # Step 1 Test world functions.
        # TDD GREEN - Make it PASS. You can ONLY make it PASS.
    def test_show_position_pass(self):
        obstacles.list_of_obstacles = []
        with captured_io(StringIO('TURTLE_BOT\nright\noff\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? TURTLE_BOT: Hello kiddo!
TURTLE_BOT: What must I do next?  > TURTLE_BOT turned right.
 > TURTLE_BOT now at position (0,0).
TURTLE_BOT: What must I do next? TURTLE_BOT: Shutting down..""", output)


        # TDD RED - Make it FAIL.
    def test_is_position_allowed_fail1(self):
        self.assertFalse(world.is_position_allowed(50, 201) == True)


    def test_is_position_allowed_fail2(self):
        self.assertFalse(world.is_position_allowed(-50, -210) == True)


    def test_is_position_allowed_fail3(self):
        self.assertFalse(world.is_position_allowed(101, 150) == True)


    def test_is_position_allowed_fail4(self):
        self.assertFalse(world.is_position_allowed(-101, -150) == True)


    def test_is_position_allowed_fail5(self):
        self.assertFalse(world.is_position_allowed(120, 250) == True)


    def test_is_position_allowed_fail6(self):
        self.assertFalse(world.is_position_allowed(-120, -250) == True)


        # TDD GREEN - Make it PASS.
    def test_is_position_allowed_pass1(self):
        self.assertTrue(world.is_position_allowed(50, 200) == True)


    def test_is_position_allowed_pass2(self):
        self.assertTrue(world.is_position_allowed(-50, -200) == True)


    def test_is_position_allowed_pass3(self):
        self.assertTrue(world.is_position_allowed(100, 150) == True)


    def test_is_position_allowed_pass4(self):
        self.assertTrue(world.is_position_allowed(-100, -150) == True)


    def test_is_position_allowed_pass5(self):
        self.assertTrue(world.is_position_allowed(90, 190) == True)


    def test_is_position_allowed_pass6(self):
        self.assertTrue(world.is_position_allowed(-90, -190) == True)


        # TDD RED - Make it FAIL.
#     def test_do_forward_fail1(self):
#         with captured_io(StringIO('TURTLE_BOT\nforward 201\noff\n')) as (out, err):
#             robot.robot_start()

#         output = out.getvalue().strip()
#         self.assertEqual("""What do you want to name your robot? TURTLE_BOT: Hello kiddo!
# TURTLE_BOT: What must I do next? TURTLE_BOT: Sorry, I cannot go outside my safe zone.
#  > TURTLE_BOT now at position (0,0).
# TURTLE_BOT: What must I do next? TURTLE_BOT: Shutting down..""", output)


    #     # TDD GREEN - Make it PASS.
    def test_do_forward_pass1(self):
        obstacles.list_of_obstacles = []
        with captured_io(StringIO('TURTLE_BOT\nforward 200\noff\n')) as (out, err):
            obstacles.random.randint = lambda a, b: 0
            robot.robot_start()

        output = out.getvalue().strip()
        self.assertEqual("""What do you want to name your robot? TURTLE_BOT: Hello kiddo!
TURTLE_BOT: What must I do next?  > TURTLE_BOT moved forward by 200 steps.
 > TURTLE_BOT now at position (0,200).
TURTLE_BOT: What must I do next? TURTLE_BOT: Shutting down..""", output)


        # TDD GREEN - Make it PASS. You can ONLY make it PASS.
    def test_do_right_turn_pass(self):
        self.assertTrue(world.do_right_turn('TURTLE_BOT') == (True, " > TURTLE_BOT turned right."))


    def test_do_left_turn_pass(self):
        self.assertTrue(world.do_left_turn('TURTLE_BOT') == (True, " > TURTLE_BOT turned left."))